import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { FormsModule } from '@angular/forms';
import { MatButtonModule, MatCardModule, MatToolbarModule,MatIconModule,MatMenuModule 
,MatGridListModule,MatTooltipModule
} from '@angular/material';
import { OverlayContainer, OverlayModule } from '@angular/cdk/overlay';
import { FieldinputComponent } from './fieldinput/fieldinput.component';
import { HttpClientModule } from '@angular/common/http';
import { GetFieldComponent } from './get-field/get-field.component';



@NgModule({
  declarations: [
    AppComponent,
    FieldinputComponent,
    GetFieldComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    FormsModule,
    OverlayModule,
    MatCardModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatGridListModule,
    MatTooltipModule,
    HttpClientModule
  ],
  exports: [MatButtonModule,
    MatCardModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatGridListModule,
    MatTooltipModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
