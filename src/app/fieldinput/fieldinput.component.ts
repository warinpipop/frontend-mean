import { Component, OnInit,Input } from '@angular/core';
import { Masterfieldinput } from '../master/mater-fieldinput';
import { GetFieldComponent } from '../get-field/get-field.component';

@Component({
  selector: 'app-fieldinput',
  templateUrl: './fieldinput.component.html',
  styleUrls: ['./fieldinput.component.sass']
})
export class FieldinputComponent implements OnInit {
  @Input()
  com1ref:GetFieldComponent;
  constructor() { }
  master: Masterfieldinput = { 
  date: new Date().toISOString(),
  firstname: '',
  lastname: ''
  }
  ngOnInit() {
  
 this.onSubmit(this.master);
  }
  onSubmit(master){
    setInterval(() => {
      master.date = new Date().toISOString()}, 1000);
      this.com1ref.postMasterfield(master)

  }
}
