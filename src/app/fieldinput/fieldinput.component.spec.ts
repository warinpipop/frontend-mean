import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldinputComponent } from './fieldinput.component';

describe('FieldinputComponent', () => {
  let component: FieldinputComponent;
  let fixture: ComponentFixture<FieldinputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FieldinputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldinputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
