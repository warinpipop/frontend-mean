import { TestBed } from '@angular/core/testing';

import { MasterfieldService } from './masterfield.service';

describe('MasterfieldService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MasterfieldService = TestBed.get(MasterfieldService);
    expect(service).toBeTruthy();
  });
});
