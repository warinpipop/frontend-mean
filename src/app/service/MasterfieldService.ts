import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class MasterfieldService {

readonly baseURL = 'http://localhost:3000/';
  constructor(private http: HttpClient) { }
  getMasterfield() {
      this.http.get(this.baseURL);
  }
}
