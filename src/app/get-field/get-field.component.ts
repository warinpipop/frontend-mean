import { Component, OnInit,Input } from '@angular/core';

import { MasterfieldService } from '../service/MasterfieldService';
import { HttpClient } from '@angular/common/http';
import { Masterfieldinput } from '../master/mater-fieldinput';
@Component({
  selector: 'app-get-field',
  templateUrl: './get-field.component.html',
  styleUrls: ['./get-field.component.sass']
})
export class GetFieldComponent implements OnInit {
 
  constructor(private masterget:MasterfieldService,private http: HttpClient ) { }
  @Input() master: any;
  ngOnInit() {
    this.getMasterfield()
    console.log(this.master.master)
  }
  readonly baseURL = 'http://localhost:3000';
  datagetfield:any;
  getMasterfield() {
      this.http.get(this.baseURL).subscribe(res=>{
        this.datagetfield=res;
        console.log(this.datagetfield)
      })
  }
  postMasterfield(Masterfieldinput){
    this.http.post(this.baseURL, Masterfieldinput).subscribe(res=>{
      console.log(JSON.stringify(res))
      this.getMasterfield();
    })
  }
  update(datajson,id){
    this.http.put(this.baseURL+'/'+id,datajson)
    .subscribe(()=>{
      this.getMasterfield();
    })
  
  }

  delete(id){
    console.log('dalete'+id)
  this.http.delete(this.baseURL+'/'+id)
  .subscribe(res=>{
    console.log(res)
    this.getMasterfield();
  })
  }

}
