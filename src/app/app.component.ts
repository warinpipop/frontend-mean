import { Component,HostBinding } from '@angular/core';
import { OverlayContainer} from '@angular/cdk/overlay';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  
constructor(public overlayContainer: OverlayContainer){}
  title = 'Class';
  @HostBinding('class') componentCssClass;
  
onSetTheme(theme) {
  this.overlayContainer.getContainerElement().classList.add(theme);
  this.componentCssClass = theme;
}


}
